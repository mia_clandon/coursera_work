const pool = require( '../../pool.js');

async function getFindServices(services = null, callback) {
    let field;

    if (services) {
        field = Number.isInteger(services) ? 'id' : 'name';
    }

    let sql = `select * from services where ${field} = ?`;

    await pool.query(sql, services, function (err, result) {
        if (err) throw err;

        if (result.length) {
            callback(result[0]);
        } else {
            callback(null);
        }
    })
}

async function createServices(body, callback) {
    let bind = [];

    for (prop in body) {
        bind.push(body[prop]);
    }

    let sql = `insert into services(name, price, description, category)values (?, ?, ?, ?)`;
    await pool.query(sql, bind, function (err, result) {
        if (err) throw err;
        callback(result.insertId);
    })
}

module.exports = {getFindServices ,createServices};
