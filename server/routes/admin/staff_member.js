const express = require('express');
const check = require('express-validator/check').check;
const validationResult = require('express-validator/check').validationResult;
const {createStaffMember, getFindStaffMember} = require("../../db/models/admin/Staff_member");

const router = express.Router();

// fullname, position, status, services
router.post('/create',
    [
        check('fullname', 'Должен быть текст').isString().isLength({min: 6}),
        check('position', 'Выберете должность сотрудника').isString().isLength({min: 1}),
        check('status', 'Выберите текущий статус сотрудника').isString().isLength({min: 1})
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Неккоректно введённые данные'
                })
            };
            const staffMemberInput = {
                fullname: req.body.fullname,
                position: req.body.position,
                status: req.body.status,
                services: req.body.services
            };

            await createStaffMember(staffMemberInput, function (lastId) {
                if (lastId) {
                    getFindStaffMember(lastId, function (result) {
                        res.status(201).json({message: 'Новый сотрудник создан'});
                    })
                } else {
                    res.status(500).json({message: 'Сотрудник с такими данными уже существует в базе данных'});
                }
            })

        } catch (e) {
            res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'});
        }
    });
