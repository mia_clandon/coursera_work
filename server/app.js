const express = require('express');
const path = require("path");
const config = require ("config");
const bodyParser = require("body-parser");
const cookieParser = require ("cookie-parser");
const logger = require("morgan");
const flash = require("express-flash");

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.json({extended: true}));

app.use('/api/admin/auth', require('./routes/admin/auth'));
app.use('/api/admin/services', require('./routes/admin/services'));
app.use('/api/admin/categories', require('./routes/admin/category'));
app.use(flash());


const PORT = 5000;

async function start() {
    try {
        await app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`))
    } catch (e) {
        console.log('Server Error', e.message);
        process.exit(1)
    }
}

start();
