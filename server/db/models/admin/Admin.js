const pool = require( '../../pool.js');
const bcrypt = require ('bcrypt');

async function getFindAdmin(admin = null, callback) {
    let field;

    if (admin) {
        field = Number.isInteger(admin) ? 'id' : 'email';
    }

    let sql = `select * from admins where ${field} = ?`;

    await pool.query(sql, admin, function (err, result) {
        if (err) throw err;

        if (result.length) {
            callback(result[0]);
        } else {
            callback(null);
        }
    })
}

async function createAdmin(body, callback) {
    let password = body.password;
    body.password = bcrypt.hashSync(password, 10);

    let bind = [];

    for (prop in body) {
        bind.push(body[prop]);
    }

    let sql = `insert into admins(username, email, password)
               values (?, ?, ?)`;
    await pool.query(sql, bind, function (err, result) {
        if (err) throw err;
        callback(result.insertId);
    })
}

async function loginAdmin(email, password, callback) {
   await getFindAdmin(email, function (admin) {
        if(admin){
            if(bcrypt.compareSync(password, admin.password)){
                callback(admin);
                return;
            }
        }

    })
}
module.exports = {getFindAdmin, createAdmin, loginAdmin};
