const express = require('express');
const check = require('express-validator/check').check;
const validationResult = require('express-validator/check').validationResult;
const pool = require("../../db/pool");
const {getFindCategories, createCategories} = require("../../db/models/admin/Category");

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const query = 'SELECT * FROM category order by id asc';
        await pool.query(query, function (err, rows) {
            res.json(rows);
        })
    } catch (e) {
        res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'})
    }
});
router.post('/create-categories',
    [
        check('category', 'Должен быть текст').isString(),
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Неккоректно введённые данные'
                })
            };
            const categoriesInput = {
                category: req.body.category,
            };

            await createCategories(categoriesInput, function (lastId) {
                if (lastId) {
                    getFindCategories(lastId, function (result) {
                        res.status(201).json({message: 'Новая категория добавлена'});
                    })
                } else {
                    res.status(500).json({message: 'Категория с таким названием уже существует в базе данных'});
                }
            })

        } catch (e) {
            res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'});
        }
    });

module.exports = router;
