const pool = require( '../../pool.js');

async function getFindCategories(services = null, callback) {
    let field;

    if (services) {
        field = Number.isInteger(services) ? 'id' : 'name';
    }

    let sql = `select * from category where ${field} = ?`;

    await pool.query(sql, services, function (err, result) {
        if (err) throw err;

        if (result.length) {
            callback(result[0]);
        } else {
            callback(null);
        }
    })
}

async function createCategories(body, callback) {
    let bind = [];

    for (prop in body) {
        bind.push(body[prop]);
    }

    let sql = `insert into category(category)values (?)`;
    await pool.query(sql, bind, function (err, result) {
        if (err) throw err;
        callback(result.insertId);
    })
}

module.exports = {getFindCategories ,createCategories};
