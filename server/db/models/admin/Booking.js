const pool = require( '../../pool.js');

async function createBookingMembers(body, callback) {
    let bind = [];

    for (prop in body) {
        bind.push(body[prop]);
    }

    let sql = `insert into booking_members(fullname, services, date, time)values (?, ?, ?, ?)`;
    await pool.query(sql, bind, function (err, result) {
        if (err) throw err;
        callback(result.insertId);
    })
}

module.exports = {createServices};
