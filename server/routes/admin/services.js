const express = require('express');
const check = require('express-validator/check').check;
const validationResult = require('express-validator/check').validationResult;
const pool = require("../../db/pool");
const {getFindServices, createServices} = require("../../db/models/admin/Services");

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const query = 'SELECT * FROM services order by id asc';
        await pool.query(query, function (err, rows) {
            res.json(rows);
        })
    } catch (e) {
        res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'})
    }
});
router.post('/create-services',
    [
        check('name', 'Должен быть текст').isString(),
        check('description', 'Должен быть текст').isString(),
        check('price', 'Укажите стоимость').isNumeric(),
        check('category', 'Укажите категорию услуг').isString(),
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Неккоректно введённые данные'
                })
            };
            const servicesInput = {
                name: req.body.name,
                price: req.body.price,
                description: req.body.description,
                category: req.body.category,
            };

            await createServices(servicesInput, function (lastId) {
                if (lastId) {
                    getFindServices(lastId, function (result) {
                        res.status(201).json({message: 'Новая услуга добавлена'});
                    })
                } else {
                    res.status(500).json({message: 'Услуга с такими данными уже существует в базе данных'});
                }
            })

        } catch (e) {
            res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'});
        }
    });

module.exports = router;
