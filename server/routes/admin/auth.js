const express = require('express');
const config = require('config');
const jwt = require('jsonwebtoken');
const check = require('express-validator/check').check;
const validationResult = require('express-validator/check').validationResult;
const {getFindAdmin, createAdmin, loginAdmin} = require("../../db/models/admin/Admin");

const router = express.Router();

//api/admin/auth/register
router.post(
    '/register',
    [
        check('email', 'Некорректный email').isEmail(),
        check('password', 'Минимальная длина пароля 6 символов')
            .isLength({min: 6})
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некоректные данные при регистрации'
                })
            }
            const adminInput = {
                username: req.body.username,
                email: req.body.email,
                password: req.body.password
            };
            await createAdmin(adminInput, function (lastId) {
                if (lastId) {
                    getFindAdmin(lastId, function (result) {
                        res.status(201).json({message: 'Пользователь создан'});
                    })
                } else {
                    res.status(500).json({message: 'Пользователь существует'});
                }
            })

        } catch (e) {
            res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'})
        }
    });
// /api/admin/auth/login
router.post(
    '/login',
    [
        check('email', 'Введите корректный email').normalizeEmail().isEmail(),
        check('password', 'Введите пароль').exists()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некорректный данные при входе в систему'
                })
            }

            const {email, password} = req.body;
            await loginAdmin(email, password, function (result) {
                if (result) {
                    let admin = `select * from admins where ${email} = ? and ${password} = ?`;
                    const token = jwt.sign(
                        { userId: admin.id },
                         config.get('jwtSecret'),
                        { expiresIn: '10h' }
                    )

                    res.json({ token, userId: admin.id })
                }
            });
        } catch (e) {
            res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'});
        }
    });
module.exports = router;

