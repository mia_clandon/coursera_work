const pool = require( '../../pool.js');

async function getFindStaffMember(staff_member = null, callback) {
    let field;

    if (staff_member) {
        field = Number.isInteger(staff_member) ? 'id' : 'fullname';
    }

    let sql = `select * from staff_members where ${field} = ?`;

    await pool.query(sql, staff_member, function (err, result) {
        if (err) throw err;

        if (result.length) {
            callback(result[0]);
        } else {
            callback(null);
        }
    })
}

async function createStaffMember(body, callback) {
    let bind = [];

    for (prop in body) {
        bind.push(body[prop]);
    }

    let sql = `insert into staff_members(fullname, position, status, services)values (?, ?, ?, ?)`;
    await pool.query(sql, bind, function (err, result) {
        if (err) throw err;
        callback(result.insertId);
    })
}

module.exports = {getFindStaffMember,createStaffMember};
